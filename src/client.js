import 'isomorphic-fetch';
import Cookie from 'js-cookie';

export default class MeosClient {
  constructor({ apiUrl, errorHandler }) {
    this.apiUrl = apiUrl;
    this.errorHandler = errorHandler;
  }

  callUrl(methodName) {
    return `${this.apiUrl}/${methodName}`;
  }

  static authToken() {
    const token = Cookie.get('meos_pc_auth_token');
    return token;
  }

  call(methodName, params) {
    return new Promise((resolve, reject) => {
      fetch(
        this.callUrl(methodName),
        {
          method: 'POST',
          headers: {
            Authorization: MeosClient.authToken(),
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(params),
        },
      )
        .then((response) => {
          if (!response.ok) {
            if (response.status === 401) {
              window.location.assign('/login');
              resolve('Redirected');
            } else if (response.status === 422) {
              return response.json().then((data) => {
                this.errorHandler({ errorKey: data.error_key, methodCalled: methodName });
                return { status: response.status, data };
              });
            }
            reject(response);
          } else {
            return response.json().then(data => ({ status: response.status, data }));
          }
          return null;
        })
        .then((response) => {
          if (response && response.data) {
            resolve(response.data);
          }
        });
    });
  }
}
