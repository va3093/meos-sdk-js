import Cookie from 'js-cookie';
import MeosClient from '../src';
import 'isomorphic-fetch';

jest.mock('js-cookie');
const apiUrl = 'http://blah.com';
const meosClient = new MeosClient({ apiUrl });

beforeEach(() => {
  jest.clearAllMocks();
  fetch.resetMocks();
});

describe('Successsful rpc call', () => {
  test('Test meos client rpc call passes through params', () => {
    // Given
    const token = 'adsf';
    Cookie.get.mockReturnValue(token);
    const funcName = 'some_func';
    const finalUrl = meosClient.callUrl(funcName);
    const params = { param1: '1' };
    const expectedResponse = { key: 'value' };
    fetch.mockResponseOnce(
      JSON.stringify(expectedResponse),
      { headers: { 'Content-Type': 'application/json' } },
    );

    // When
    return meosClient.call(funcName, params)
      .then((data) => {
        // Then
        expect(fetch).toBeCalledWith(
          finalUrl,
          {
            body: JSON.stringify(params),
            headers: {
              Authorization: token,
              'Content-Type': 'application/json',
            },
            method: 'POST',
          },
        );
        expect(data).toEqual(expectedResponse);
      });
  });
});

describe('Unsuccessful rpc call', () => {
  test('Test meos client throws error when http error', () => {
    // Given
    const funcName = 'some_func';
    const params = { param1: '1' };
    const errorText = 'There was an error';
    const localStatus = 500;
    fetch.mockResponseOnce(
      errorText,
      { status: localStatus },
    );

    // When
    return meosClient.call(funcName, params)
      .catch((error) => {
        // Then
        expect(error.status).toEqual(localStatus);
        expect(error.body).toEqual(errorText);
      });
  });

  test('Test 422s call the errorHandler', () => {
    // Given
    const funcName = 'some_func';
    const localErrorKey = 'error_k';
    const localStatus = 422;
    const errorHandler = jest.fn();
    const localMeosClient = new MeosClient({ apiUrl: 'http://blah.com', errorHandler });
    fetch.mockResponseOnce(
      JSON.stringify({ error_key: localErrorKey, method_called: funcName }),
      { status: localStatus },
    );

    // When
    return localMeosClient.call(funcName, {})
      .then(() => {
        expect(errorHandler).toBeCalledWith({ errorKey: localErrorKey, methodCalled: funcName });
      });
  });
});

describe('Unauthorized request', () => {
  // beforeAll(() => {
  //   window.history.pushState({}, 'Test Title', '/test.html?query=true');
  // });

  test('Ensure user is redirected to login page', () => {
    // Given
    const funcName = 'some_func';
    const params = { param1: '1' };
    const localStatus = 401;
    window.location.assign = jest.fn();
    fetch.mockResponseOnce(
      '{"reason": "unauthorised"}',
      { status: localStatus },
    );

    // When
    return meosClient.call(funcName, params)
      .then(() => {
        // Then
        expect(window.location.assign).toBeCalledWith('/login');
      });
  });
});
